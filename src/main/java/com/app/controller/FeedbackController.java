package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.FeedbackService;

@RestController
@RequestMapping("/v1/feedback")

public class FeedbackController {

	@Autowired
	private FeedbackService feedbackService;
	
	@GetMapping()
	public String getmessage() {
		return "hello";
	}
	
	@GetMapping("/{fileName}")
	public String addfileAndMessage(@PathVariable String fileName ,@RequestParam String message) {
		return feedbackService.addFileAndMassage(fileName, message);
	}
}
